defmodule Cashy.Bug2 do
  def convert(:chf, :usd, amount) do
    {:ok, amount * 1.01}
  end

  def convert(_, _, _) do
    {:error, :invalid_amount}
  end

  def run(amount) do
    case convert(:chf, :usd, amount) do
      {:ok, amount} ->
        IO.puts "converted amount is #{amount}"
      {:error, reason} ->
        IO.puts "whoops, #{String.to_atom(reason)}"
    end
  end
end
