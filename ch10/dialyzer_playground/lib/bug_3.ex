defmodule Cashy.Bug3 do
  def convert(:chf, :usd, amount) when amount > 0 do
    {:ok, amount * 1.01}
  end

  def run(amount) do
    case convert(:chf, :usd, amount) do
      amount when amount <= 0 ->
        IO.puts "whoops, should be more than zero"
      _ ->
        IO.puts "converted amount is #{amount}"
    end
  end
end
