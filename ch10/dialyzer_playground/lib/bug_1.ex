defmodule Cashy.Bug1 do
  def convert(:chf, :usd, amount) do
    {:ok, amount * 1.01 }
  end

  def run do
    convert(:chf, :usd, :one_million_dollars)
  end
end
