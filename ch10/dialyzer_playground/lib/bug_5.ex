defmodule Cashy.Bug5 do
  @type currency() :: :chf | :usd

  @spec convert(currency, currency, number) :: number
  def convert(:chf, :usd, amount) do
    amount * 1.01
  end

  @spec amount({:value, number}) :: number
  def amount({:value, value}) do
    value
  end

  def run do
    convert(:sgd, :usd, amount({:value, :one_million_dollars}))
  end
end
