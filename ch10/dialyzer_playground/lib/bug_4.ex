defmodule Cashy.Bug4 do
  def convert(:chf, :usd, amount) when is_float(amount) do
    {:ok, amount * 1.01}
  end
  def run do
    convert(:sgd, :usd, 10)
  end
end
