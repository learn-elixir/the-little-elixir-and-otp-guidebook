defmodule Exercises do
  # Exercise 1
  def sum([]), do: 0
  def sum([head|tail]) do
    sum(head) + sum(tail)
  end
  def sum(head), do: head

  # Exercise 3
  def transform_without_pipes(list) do
    Enum.map(Enum.reverse(List.flatten(list)), fn x -> x * x end)
  end
  def transform_with_pipes(list) do
    list |> List.flatten |> Enum.reverse |> Enum.map(fn x -> x * x end)
  end
  
  # Exercise 4
  def crypto() do
    :crypto.md5("Tales from the Crypt")
  end
  def main do
    IO.puts sum([1, 2, 3, 4, 5])
    IO.puts Enum.sum([1, 2, 3, 4, 5])

    IO.inspect transform_without_pipes [1,[[2],3]]
    IO.inspect transform_with_pipes [1,[[2],3]]
  end
end
