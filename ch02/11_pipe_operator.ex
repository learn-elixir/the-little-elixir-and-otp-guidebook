defmodule PipeOperator do
  def do_request(html) do
    IO.inspect html
  end
  def start_no_pipes(url) do
    do_request(HTTPoison.get(url))
  end
  def start(url) do
    url |> HTTPoison.get |> do_request
  end
  def find_books() do
    "/Users/Edu/Books"
    |> Path.join("**/*.epub")
    |> Path.wildcard
    |> Enum.filter(fn fname ->
      String.contains?(Path.basename(fname), "Java")
    end)
  end
end
