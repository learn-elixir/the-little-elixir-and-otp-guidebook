defmodule Lists do
  def sample() do
    IO.inspect [1, 2, 3] = [1 | [2 | [3 | []]]]

    [head | tail] = [1, 2, 3]
    IO.puts head
    IO.inspect tail

    list = [1, 2, 3]
    IO.inspect list
    IO.inspect [0 | list ]
    IO.inspect [0] ++ list

    [head | tail] = [:lonely]
    IO.inspect head
    IO.inspect tail

    IO.inspect List.flatten [1, [:two], ["three", []]]

    Lists.MyList.flatten [1, [:two], ["three", [4, []]]]
  end

  defmodule MyList do
    def flatten([]), do: []
    def flatten([head | tail]) do
      flatten(head) ++ flatten(tail)
    end
    def flatten(head), do: [head]
  end
end
