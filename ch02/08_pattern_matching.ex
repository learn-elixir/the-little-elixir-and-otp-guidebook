defmodule Patterns do
    def matching() do
        programmers = Map.new
        programmers = Map.put(programmers, :joe, "Erlang")
        programmers = Map.put(programmers, :jose, "Elixir")
        programmers = Map.put(programmers, :matz, "Ruby")
        programmers = Map.put(programmers, :rich, "Clojure")
        
        IO.inspect %{joe: "Erlang", jose: "Elixir", matz: "Ruby", rich: "Clojure"}
 = programmers

        # distructuring
        %{joe: a, jose: b, matz: c, rich: d} =
     %{joe: "Erlang", jose: "Elixir", matz: "Ruby", rich: "Clojure"}
        IO.puts a
        IO.puts b
        IO.puts c
        IO.puts d

        %{jose: most_awesome_language} = programmers
        IO.inspect most_awesome_language

        IO.inspect Map.fetch(programmers, :rich)
        IO.inspect Map.fetch(programmers, :rasmus)

        case Map.fetch(programmers, :rich) do
            {:ok, language} ->
                IO.puts "#{language} is a legit language."
            :error ->
                IO.puts "No idea what language this is."
        end
    end

    def check_board(board) do
  case board do
    { :x, :x, :x,
      _ , _ , _ ,
      _ , _ , _ } -> :x_win
    { _ , _ , _ ,
      :x, :x, :x,
      _ , _ , _ } -> :x_win

    { _ , _ , _ ,
      _ , _ , _ ,
      :x, :x, :x} -> :x_win

    { :x, _ , _ ,
      :x, _ , _ ,
      :x, _ , _ } -> :x_win

    { _ , :x, _ ,
      _ , :x, _ ,
      _ , :x, _ } -> :x_win

    { _ , _ , :x,
      _ , _ , :x,
      _ , _ , :x} -> :x_win

    { :x, _ , _ ,
      _ , :x, _ ,
      _ , _ , :x} -> :x_win

    { _ , _ , :x,
      _ , :x, _ ,
      :x, _ , _ } -> :x_win

    # Player O board patterns omitted ...

    { a, b, c,
      d, e, f,
      g, h, i } when a and b and c and d and e and f and g and h and i -> :draw

    _ -> :in_progress

  end
end
