defmodule DataTypes do
    def numbers() do
        IO.puts 1 + 0x2F / 3.0
        IO.puts div(10, 3)
        rem(10, 3)
    end
    def strings() do
        IO.puts "Strings are #{:great}!"
        IO.puts "Strings are #{:great}!" |> String.upcase |> String.reverse
        IO.puts "Strings are #{:great}!" |> is_binary
        
        IO.puts "ohai" <> <<0>>
        
        IO.puts ?o
        IO.puts ?h
        IO.puts ?a
        IO.puts ?i

        IO.puts 'ohai' == "ohai"
    end
    def atoms() do
        IO.puts :hello_atom == "hello_atom"
    end
    def tuples() do
        IO.inspect {200, "http://www.elixir-lang.org"}
        IO.puts elem({404, "http://www.php-is-awesome.org"}, 1)
        put_elem({404, "http://www.php-is-awesome.org"}, 0, 503)
    end
    def maps() do
        programmers = Map.new
        IO.inspect programmers
        programmers = Map.put(programmers, :joe, "Erlang")
        IO.inspect programmers
        programmers = Map.put(programmers, :matz, "Ruby")
        IO.inspect programmers
        programmers = Map.put(programmers, :rich, "Clojure")
        IO.inspect Map.put(programmers, :rasmus, "PHP")
        programmers
    end
end
