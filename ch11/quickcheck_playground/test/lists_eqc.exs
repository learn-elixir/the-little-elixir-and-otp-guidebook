defmodule ListsEQC do
  use ExUnit.Case
  use EQC.ExUnit

  property "reversing a list twice yields the original list" do
    forall l <- list(int()) do
      ensure l |> Enum.reverse |> Enum.reverse == l
    end
  end

  property "encoding is the reverse of decoding" do
    forall bin <- binary do
      ensure bin |> Base.encode64 |> Base.decode64! == bin
    end
  end

  def is_sorted([]), do: true
  def is_sorted(list) do
    list
    |> Enum.zip(tl(list))
    |> Enum.all?(fn {x, y} -> x <= y end)
  end

  property "sorting works" do
    forall l <- list(int) do
      ensure l |> Enum.sort |> is_sorted == true
    end
  end

  property "appending an element and sorting it is the same as prepending an element and sorting it" do
    forall {i, l} <- {int, list(int)} do
      [i|l] |> Enum.sort == l ++ [i] |> Enum.sort
    end
  end

  property "calling Enum.uniq/1 twice has no effect" do
    forall l <- list(int) do
      ensure l |> Enum.uniq == l |> Enum.uniq |> Enum.uniq
    end
  end

  # skip generated case which fails condition
  property "tail of list with implies" do
    forall l <- list(int) do
      implies l != [] do
        [_head|tail] = l
        ensure tl(l) == tail
      end
    end
  end

  # don't generate cases, which aren't interesting for the test
  property "tail of list" do
    forall l <- non_empty(list(int)) do
      [_head|tail] = l
      ensure tl(l) == tail
    end
  end

  property "list concatenation" do
    forall {l1, l2} <- {list(int), list(int)} do
      ensure Enum.concat(l1, l2) == l1 ++ l2
    end
  end

  def nested_list(gen) do
    sized size do
      nested_list(size, gen)
    end
  end

  defp nested_list(0, _gen) do
    []
  end

  defp nested_list(n, gen) do
    lazy do
      oneof [[gen|nested_list(n-1, gen)],
        [nested_list(n-1, gen)]]
    end
  end

  def balanced_tree(gen) do
    sized size do
      balanced_tree(size, gen)
    end
  end

  def balanced_tree(0, gen) do
    {:leaf, gen}
  end

  def balanced_tree(n, gen) do
    lazy do
      {:node,
        gen,
        balanced_tree(div(n, 2), gen),
        balanced_tree(div(n, 2), gen)}
    end
  end
end
