defmodule StringEQC do
  use ExUnit.Case
  use EQC.ExUnit

  property "splitting a string with a delimiter and joining it again yields the same string" do
    forall s <- list(char()) do
      s = to_string(s)
      ensure String.split(s, ",") |> join(",") == s
    end
  end

  property "splitting a string with a delimiter and joining it again yields the same string with report" do
    forall s <- list(char()) do
      s = to_string(s)
      :eqc.classify(String.contains?(s, ","),
        :string_with_commas_1,
        ensure String.split(s, ",") |> join(",") == s)
    end
  end

  property "splitting a string with a delimiter and joining it again yields the same string with custom generator" do
    forall s <- string_with_commas do
      s = to_string(s)
      :eqc.classify(String.contains?(s, ","),
        :string_with_commas_2,
        ensure String.split(s, ",") |> join(",") == s)
    end
  end

  def string_with_commas do
    let len <- choose(10, 20) do
      let string <- vector(len,
        frequency([{10, oneof(:lists.seq(?a, ?z))},
          {2 , ?,}])) do
        string
      end
    end
  end

  defp join(parts, delimiter) do
    parts |> Enum.intersperse([delimiter]) |> Enum.join
  end
end