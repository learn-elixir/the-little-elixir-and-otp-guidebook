defmodule Pingpong.Pong do
  def loop do
    receive do
      {sender_pid, pong} ->
        if pong == "pong" do
          IO.puts "ping"
          send sender_pid, {self(), "ping"}
          loop()
        end
      _ ->
        IO.puts "Unknown message in Pong"
    end
  end
end
