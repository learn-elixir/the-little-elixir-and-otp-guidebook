defmodule Pingpong do
  @moduledoc """
  Documentation for Pingpong.
  """

 def pingpong do
    ping_pid = spawn(Pingpong.Ping, :loop, [])
    pong_pid = spawn(Pingpong.Pong, :loop, [])
    send ping_pid, {pong_pid, "ping"}
  end
end
