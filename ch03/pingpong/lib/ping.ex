defmodule Pingpong.Ping do
  def loop do
    receive do
      {sender_pid, ping} ->
        if ping == "ping" do
          IO.puts "pong"
          send sender_pid, {self(), "pong"}
          loop()
        end
      _ ->
        IO.puts "Unknown message in Ping"
    end
  end
end
